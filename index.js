let indexValue = 0;
const changeImages = () => {
    const images = document.querySelectorAll(".image-to-show");

    images.forEach((img) => {
        img.style.opacity = "0";
    });
    indexValue++;

    if (indexValue > images.length) {
        indexValue = 1;
    }

    images[indexValue - 1].style.opacity = "1";
};

let slideShow = setInterval(changeImages, 3000);

const manipulateSlideShow = () => {
    const stopButton = document.querySelector(".stop-btn");
    const continueButton = document.querySelector(".continue-btn");

    continueButton.disabled = true
    continueButton.style.opacity = "0.6"

    stopButton.addEventListener("click", () => {
        clearInterval(slideShow);
        continueButton.disabled = false
        stopButton.disabled = true
        continueButton.style.opacity = "1"
        stopButton.style.opacity = "0.6"
    });

    continueButton.addEventListener("click", () => {
        slideShow = setInterval(changeImages, 3000);
        continueButton.disabled = true
        stopButton.disabled = false
        continueButton.style.opacity = "0.6"
        stopButton.style.opacity = "1"
    });
};

manipulateSlideShow();

